rocas.docx : rocas.md reference.docx
	pandoc --reference-doc=reference.docx rocas.md -o rocas.docx

rocas.pdf : rocas.md
	pandoc --output=rocas.pdf rocas.md
