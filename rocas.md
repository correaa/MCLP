# Aplicación de rocas ígneas de composición media riolítica procesadas por atrición seca en la producción de materiales cerámicos de alta densidad por colado (vía húmeda). Parte II. Optimización de tiempos de molienda

Correa, A. [1]; Rovere,  E. [2]; Fuertes, L.[3], Sánchez, L.[1], Bruno, O.[1]

[1] CIPROMIN, INTEMIN-SEGEMAR, Av. General Paz 5445, San Martín (1650), Buenos Aires, Argentina

[2] DGAyA, IGRM-SEGEMAR, Av. General Paz 5445, San Martín (1650), Buenos Aires, Argentina

[3] CIDEMAT, INTEMIN-SEGEMAR, Av. General Paz 5445, San Martín (1650), Buenos Aires, Argentina

_Palabras clave: Gres, Cinética de Molienda de alta energía, atrición._

_Keywords: Gres; High energy grinding kinetics, attrition._

## RESUMEN

En el presente trabajo se estudió la molienda batch de alta energía en seco de un canto rodado del Río Santa Cruz, Argentina.
La molienda de alta energía sobre esta roca tiene por objetivo lograr una adecuada conformación cerámica con una arcilla comercial, se evita así el uso de otros componentes como cuarzo y feldespato.
Se procede a un estudio de la cinética, basándose en el modelo destructivo total de Kelsall en sus aspectos asintóticos;
en particular en el comportamiento a tiempos largos, superiores a 30minutos en donde el mecanismo de atrición predomina.
Se determina que los parámetros de escalamiento adecuados de tiempo deberían estar entre 50 y 60 minutos ya que se obtienen granulometrías de 27 y 35 % pasante en masa el diámetro de 2 µm.

## ABSTRACT

A study of a high-energy batch dry grinding has been performed in the present work on a rolling stone of the Santa Cruz River, Argentina.
The idea of applying high-energy grinding in this rock is to achieve a suitable ceramic conformation with regular clay, avoiding using other commercial ingredients such as quartz and feldspars. We produced a kinetics study based on Kelsall’s complete destructive model;
putting the stress on its asymptotic aspects, particularly in its long-time behavior;
i.e., longer than 30minutes where the attrition mechanism prevails.
We have learned that the scale-up parameters must be located between 50 and 60 minutes because granulometric distributions of 27 and 35 % in weight lesser than 2 µm diameter are achieved.

## INTRODUCCIÓN

En lo sus últimos 30 años ha tomado impulso la molienda drástica de materiales, materias primas incluso de rocas.
Se logra así que rocas de escasa maleabilidad al reducirse drásticamente su tamaño permiten una conformación adecuada con arcillas, reemplazando a minerales comerciales de costosa extracción.
Con rocas como los basaltos se logra la fabricación de fibras las que introducidas en los cementos evitan la corrosión de las neblinas salinas.
Otros fenómenos importantes se producen cuando al utilizar moliendas drásticas se producen cambios estructurales que incluye el deformación y eliminación de estructuras cristalinas con el fin de activar especies como zeolita, caolines ya sea para incrementar sus capacidades de intercambio en procesos de adsorción, incrementar la velocidad de cinética en la lixiviación y el fortalecimiento de las estructuras en los cementos.
En este fenómeno las aleaciones pueden obtenerse como en el caso de las reacciones $\mathrm{TiO_2-Al_2O_3}$.

En escala industrial estos procesos se llevan a cabo en molinos verticales u horizontales denominados _attritors_ (erosionadores); con cargas de bolas cuyo material se elige de acuerdo al sólido que se quiere moler: densidad distribución granulométrica de entrada y de salida que se quiere obtener.
También se han incorporados los equipos llamados planetarios en donde a similitud del sistema solar los recipientes donde se realiza la molienda giran sobre un disco centrado en un eje.
El los recipientes de molienda gira sobre sí mismo en el mismo sentido u opuesto al del disco se obtiene una composición de velocidades de rotación que produce efectos como la aparición de aceleración de Coriolis e intensivas frecuencias de choque entre partículas, entre partículas bolas y partículas paredes del recipiente.
El proceso puede realizarse en seco o en un medio líquido en donde se abre la posibilidad de la dosificación de agentes dispersión como hexameta-fosfato de sodio, variedades de silicato de sodio, poliacrilatos etcétera.

El otro mecanismo es el de erosión o atrición; es más lento; ocurre por acreción; la partícula va disminuyendo de tamaño en forma continua atravesando las fronteras de medida. 
También produce pequeñas partículas que generalmente concurren a los tamaños inferiores. 
Éstas pueden ser responsables de iniciar procesos de *ripening*.

como consecuencia del brusco aumento tanto del número de partículas, como el del aumento de la superficie geométrica de las partículas.

Pueden producirse reacciones no deseadas e incontrolables dada la energía superficial que se pone en juego y la eventual ocurrencia de soldaduras en las granulometrías cercanas al 1 µm, excesiva nucleación, o nucleación súbita. 
Hay otros dos mecanismos intermedios como *chipening*, eliminación de las aristas, *indentation* (acción de una fuerza concentrada sobre una superficie).
La erosión prevalece sobre los mecanismos restantes en los tiempos largos.

## OBJETIVOS

El objetivo de este trabajo en especial es por medio de la molienda seca de alta energía lograr que una andesita pueda utilizarse en la producción de cerámicos de alta densidad incluso en esmaltes en esa disciplina.
Esta materia prima por su composición puede sustituir a materias primas tradicionales como cuarzos y feldespatos.
El bajo costo de extracción de esa andesita contrasta con el gran consumo de energía en la molienda y el costo de los equipos del proceso.
Por lo tanto es necesario el conocimiento de esa operación que incluye el estudio de la cinética en ensayos preferentemente del tipo batch, con la consecuente optimización de los tiempos de molienda y lograr así un correcto escalamiento en escala industrial.
El bajo costo de extracción de esa andesita contrasta con el gran consumo de energía en la molienda y el costo de los equipos del proceso.
Por lo tanto es necesario el conocimiento de esa operación que incluye el estudio de la cinética en ensayos preferentemente del tipo batch, con la consecuente optimización de los tiempos de molienda y lograr así un correcto escalamiento en escala industrial.

## MATERIALES Y MÉTODOS

Generalmente estos estudios se basan en la aplicación de ecuaciones integrales de fractura y erosión.

Al aplicar estas ecuaciones a un sistema de molienda nos encontramos en dificultades, sobre todo en el cálculo de las matrices de selección y rotura.

Sin embargo, el carácter heurístico que de ellas emana da una descripción adecuada del proceso. 

En este caso en cambio la cinética de la molienda se estudiará sobre la base del modelo propuesto por Kelsall en los procesos de flotación de tres parámetros.

Este modelo es del tipo destructivo y se aplica a la función de distribución granulométrica en masa expresada como $R(x)$;

(@label1) $$ R(x,t) = \{1-Q(x)\} \exp[-K_r(x) t] + Q(x) \exp[-K_a(x) t] $$

Donde

$R(x,t)$: Función acumulativa en masa  pasante o función de supervivencia asociada al tamaño $x$ (µm) en el  tiempo $t$ (minutos).

$Q(x)$: Fracción en masa sometida a la erosión de $R(x,t)$  

$K_a(x)$: Constante específica de tasa de destrucción de la fracción sometida a la erosión en $R(x,t)$ (1/minuto)

$K_r(x)$: Constante específica de tasa de destrucción de la fracción sometida a la fractura en $R(x,t)$ (1/minuto)

Para que estas situaciones sean más fáciles de describir se efectúan los ensayos a partir de una fracción de tamaño grueso limitada su granulometría en un intervalo conocido en sus extremos superior e inferior.
Esto permite afirmar que todas las partículas del sistema que aparezcan en un tamaño inferior al del intervalo definido provendrán de esta fracción primordial.
En este caso cuando se quiere describir la cinética de destrucción de la función de supervivencia es $R(x,0) = 1$ para todo diámetro $x$ menor al límite inferior de la granulometría inicial.

Cuando los resultados obtenidos se ubican en el plano $\ln R(x, t)$ vs. $t$ además de modelarse por minimización de errores; ecuación (1) también cabe utilizar la modelación asintótica.
Esto consiste en determinar tramo lineal descendente en el instante inicial se toma como el efecto de todos los mecanismos de conminución cuyo mecanismo predominante es el de la fractura en el posible caso que los hubiere. Tal como se muestra en la Figura 1.
Nos dará una pendiente que denominaremos $-K_o$ (1/minuto).
Por  otro lado, en los tiempos largos se postula que todos los mecanismos dejan de actuar y queda sólo el mecanismo de atrición que se modela como lo indica la ecuación 2.
Nótese que en el campo semilogarítmico es una recta de pendiente $K_a$. 

$$
R(x,t) = Q(x) \exp[-K_a(x) t]
$$

![Fig. 1. Comportamiento asintótico. Modelo de Kelsall.](fig1.png){width=80%}

Para caracterizar las granulometrías a través del tiempo se efectuaron modelaciones de las mismas.

La distribución adoptada fue la llamada distribución tangente hiperbólica interna de dos parámetros.

De ese modo se pudo obtener en forma analítica los diámetros medios, mediano, la desviación estándar (sigma) o sea la dispersión de la distribución el coeficiente de variación (CV)  y skewness II de Pearson para evaluar la simetría,



$$
F(x) = \tanh\left[ \left(\frac{x}{p} \right)^n \right]
$$

$F(x)$: Función cumulativa retenida (CDF)

$n$: Exponente

$p$: Diámetro de referencia (µm)

La función distribución CDF está vinculada a la distribución de sobrevivencia $R(x)$ de modo que

$$
F(x) = 1 - R(x)
$$

Los parámetros CV y *skewness* de Pearson o mediana quedan definida por

$$
\text{CV} = \frac{\text{sigma}}{\text{diámetro medio}}
$$

$$
\text{skewness} = \frac{3 \times (\text{diámetro mediano} - \text{diámetro medio})}{\text{sigma}}
$$

La roca a procesar es una Andesita proveniente de la naciente s del Río Santa Cruz provincia de Santa Cruz Argentina. La descripción de este canto rodado se realiza en la Parte I de ese trabajo.

En estos ensayos se partió de una muestra de estrecha distribución de tamaño entre 300 y 600µm que se obtuvo con equipos de molienda de mandíbulas y de rodillo a partir de piezas de tamaño puño. 

Se realizaron ensayos batch en escala laboratorio con la utilización de un molino planetario Retsch modelo PM400.
El equipo presenta un disco rotatorio de 46,8cm de diámetro y sobre él se apoyan cuatro recipientes ubicados en ángulo recto de 7cm de altura, de 0,5litros de volumen y de un radio interno de 4,9cm cuyos centros se encuentra a una distancia radial del centro de giro del disco a 15,8cm.
Los recipientes giran una vuelta sobre sí mismos cuando el disco completa una revolución. Los ensayos se realizaron a 200v.p.m. del disco que implica la suma vectorial de dos rotaciones;
la del disco a 200v.p.vm con radio de 0,158m y los cilindros. con un radio de rotación de 0,049 m.
Se utilizaron con el material de estudio sólo dos de los cuatro recipientes con un recubrimiento interior y bolas de silicona de una densidad de 6000kg/m3 con una masa total de 482 g; compuesto por 5 bolas de 2,45mm, 19 bolas de 5,3mm y 52 de 0,71mm de diámetro. Se realizaron ensayos a 10, 20, 30, 50 y 60minutos.
A las muestras obtenidas les fueron analizadas sus granulometrías con un equipo Sedigraph III-5120; y cuando fue necesario se utilizaron técnicas en base a tamices ASTM con un corte en un tamiz de 63µm (230 ASTM).
Se modelaron las granulometrías de acuerdo la función distribución elegida; se obtuvieron los parámetros que definen la distribución:  $p$ y $n$ y los datos estadísticos como media, mediana, sigma y *skeweness* Pearson. 

Con estos datos de las granulometrías se modeló la cinética de la molienda sobre la ecuación de Kelsall; se obtuvieron para cada tamaño propuesto los parámetros cinéticos $Q(x)$, $K_a(x)$ y $K_o(x)$. 

## RESULTADOS

Los parámetros que caracterizan las granulometrías obtenidas se observan en la Tabla I.
En la Figura 2 puede observarse en forma gráfica la distribución de la granulometría a distintos tiempos de molienda.
En la Figura 3 se muestran la variación de $R(x,t)$ en función del tiempo; modelo de Kelsall.

TABLA I. Granulometría global según el tiempo de molienda.

| Tiempo de molienda (minutos) | 0    | 10    | 20   | 30   | 50   | 60   |
| ---------------------------- | ---- | ----- | ---- | ---- | ---- | ---- |
| Media (µm)                   | 450  | 22,4  | 16,7 | 9,6  | 8,7  | 7,2  |
| Mediana (µm)                 | 450  | 44    | 6,7  | 5,7  | 5,3  | 3,9  |
| CV                           | 0,19 | 1,51  | 1,59 | 1,17 | 1,15 | 1,27 |
| Skw mediana                  | 0    | -1,92 | 1,13 | 1,03 | 1,02 | 1,07 |
| Sigma (µm)                   | 86,6 | 33,8  | 26,7 | 11,3 | 10,1 | 9,1  |

![Fig. 2. $R(x, t)$ vs. $x$ a distintos tiempos de molienda.](fig2.png){width=80%}

![Fig. 3. $R(x, t)$ vs. tiempo.](fig3.png){width=80%}

Las constantes cinéticas de los diámetros de las diferentes R(x).
En la Figura 5 se observa el parámetro $Q(x)$ vs. el diámetro de las diferentes $R(x,t)$

TABLA II. Parámetros cinéticos de cinética Kelsall.

|     Diámetro    |     $K_o$    |     $K_s$    |     $K_r$    |   $Q$         |
|-----------------|--------------|--------------|--------------|---------------|
|     µm          |     1/min    |     1/min    |     1/min    |     -         |
|     300         |     0,691    |     n.a      |     0,691    |     0         |
|     150         |     0,407    |     n.a      |     0,407    |     0         |
|     106         |     0.155    |     n.a      |     0,155    |     0         |
|     75          |     0,094    |     0,069    |     0,094    |     0,0002    |
|     53          |     0,076    |     0.034    |     0,076    |     0,0007    |
|     45          |     0,071    |     0,026    |     0,071    |     0,0021    |
|     38          |     0,066    |     0,030    |     0,066    |     0,0028    |
|     23          |     0,053    |     0,030    |     0,053    |     0,0100    |
|     20          |     0,050    |     0,031    |     0,050    |     0,0126    |
|     12          |     0,038    |     0,026    |     0,039    |     0,0384    |
|     10          |     0,034    |     0,024    |     0,035    |     0,0548    |
|     6           |     0,026    |     0,019    |     0,027    |     0,1214    |
|     5           |     0,023    |     0,017    |     0,024    |     0,1530    |
|     3           |     0,017    |     0,013    |     0,019    |     0,2600    |
|     2           |     0,014    |     0,010    |     0,015    |     0,3529    |
|     1           |     0,009    |     0,007    |     0,011    |     0,5108    |

![Fig. 4. $K_o(x)/K_a(x)$ vs. diámetro (µm).](fig4.png){width=80%}

![Fig. 5. $Q(x)$ vs. diámetro de fracción retenida.](fig5.png){width=80%}

![Fig. 6. Diámetro medio mediano y sigma vs. tiempo.](fig6.png){width=80%}

## DISCUSIÓN

Los ensayos realizados muestran que se alcanzar a granulometrías que son compatibles para producir una mezcla más que aceptables con una arcilla tipo Tinkar.
De acuerdo a lo esperado se observan los que en los momentos iniciales en los tamaños mayores se producen una disminución brusca de la función $R(x)$, indicando la preponderancia de los procesos de fractura.
Luego a partir de los 30 minutos las fracciones de mayor tamaño a 38µm son indetectables.
Las constantes cinéticas tanto de erosión como las iniciales están en relación directa con el tamaño de partícula.
La fracción de erosión empieza a crecer por debajo de los 9µm como se ve en la Figura 5.
El comportamiento de sigma y skewness es típica de estas moliendas cuando se parte desde una granulometría cerrada que se considera de absoluta simetría, skewness nula.
Disminuye en los primeros momentos y luego se hace francamente positiva cuando se consigue eliminar los tamaños mayores.
Si bien se observan variaciones en la desviación estándar puede observarse que el coeficiente de variación (CV) es cuasi constante.
Se observa que a tiempos largos el mecanismo de erosión prevalece sobre los demás mecanismos.

## CONCLUSIÓN

El modelo cinético de Kelsall se muestra como una herramienta robusta para describir estos procesos y efectuar diagnósticos.
Los productos a utilizar parecen situarse a tiempos ubicados entre 50 y 60 minutos. De hecho, la conformación cerámica se realizó con la roca molida después de 60 minutos de molienda, ya que presentaban granulometría con pasante los2µm entre 27 y 35% en masa.
Se comprueba el comportamiento asintótico del sistema a bajos y largos tiempos.
Los futuros trabajos con relación estarán orientados a entregar roca molida a distintos tiempos de molienda y evaluar las respuestas en las conformaciones cerámicas.
Por otro es adecuado estudiar la granulometría por debajo de 1µm con equipos tipo Laser nanosizer para ... 

## AGRADECIMIENTOS

Los autores agradecen el soporte de sus respectivas instituciones y guía de sus mentores.

## REFERENCIAS

**(Bertin 2016)** D. Bertin, I, Cotabarren, J, Piña, V. Bucalá.”Population balance discretization for growth, attrition, aggregation, breakage and nucleation” Computers & Chemical Engineering. 84 (January),132-150. 2016

**(Bickel 1998)** T. Bickel,B. Lakatos, C.Mihálikó,Z.Ulbert.”The hyperbolic tangent distribution family”. Powder Technology”. 97,2, 100-108. 1998

**(Herbst 1980)** J. Herbst, D. Fuerstenau. “Scale-up procedure for continuous grinding mill design using population balance model”. International journal of mineral processing 7,1-31, 1980.

**(Kelsall 1961)** D. Kelsall “Application of probability assessment of flotation systems.”. Bull,Inst,Metal, 650, 191-204, 1961

**(King 1972)** King, R. “An Analytical Solution to the batch comminution equation”. Journal of the South African Institute of Mining and Metallurgy.73,4,127-131, 1972
