Instrucciones:

- Editar (online) el archivo `rocas.md`
- El archivo no se edita con Word, sino con puro texto online.
- Para grabar, ir al botón azul "Commit changes" (Target Branch debe quedar en "main")
- Volver a la página principal del proyecto, ir al botón gris y verde que dice "Download DOCX"
- Iterar el texto y, si es necesario, las imagenes (se puede hacer upload en formato PNG)
- Bajar el archivo final con el botón gris y verde que dice "Download DOCX"
